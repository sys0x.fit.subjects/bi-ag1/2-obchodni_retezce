#ifndef __PROGTEST__
#include <cassert>
#include <algorithm>

#endif /* __PROGTEST__ */

using namespace std;
class Item{
private:
    bool isAliv;
    unsigned int itemID;
    unsigned int revenue;
    unsigned  long long int timeOfItemCreation;
public:
    Item(Item const &item) : isAliv(item.isAliv), itemID(item.itemID), revenue(item.revenue), timeOfItemCreation(item.timeOfItemCreation) {}

    Item(unsigned int itemID, unsigned int revenue, unsigned  long long int time,bool isAlive) :  isAliv(isAlive), itemID(itemID), revenue(revenue), timeOfItemCreation(time) {}

    bool operator==(const Item &rhs) const {
        return itemID == rhs.itemID &&
               revenue == rhs.revenue &&
               timeOfItemCreation == rhs.timeOfItemCreation;
    }

    bool operator!=(const Item &rhs) const {
        return !(rhs == *this);
    }
    bool revenueEqualsorSmaler(const Item &rhs)const {
        return revenue == rhs.revenue || revenue > rhs.revenue;

    }
    bool operator<(const Item &rhs) const {
        if(revenue < rhs.revenue)
            return revenue < rhs.revenue;
        else if (revenue > rhs.revenue)
            return revenue < rhs.revenue;
        else if (itemID<rhs.itemID)
            return itemID<rhs.itemID;
        else if (itemID>rhs.itemID)
            return itemID<rhs.itemID;
        else return timeOfItemCreation < rhs.timeOfItemCreation;

    }
    bool operator>(const Item &rhs) const {
        return rhs < *this;
    }
    bool operator<=(const Item &rhs) const {
        return !(rhs < *this);
    }
    bool operator>=(const Item &rhs) const {
        return !(*this < rhs);
    }

    unsigned int getItemId() const {
        return itemID;
    }
    unsigned int getRevenue() const {
        return revenue;
    }
    bool isAlive(){
        return isAliv;
    }

};
class Node{
private:
    Item data;
    Node * parent = nullptr;
    Node * neighbour = nullptr;
    Node * prevNeighbour= nullptr;
    Node * child = nullptr;
    int order=0;
public:
    int getOrder() const {
        return order;
    }

    void setOrder(int order) {
        Node::order = order;
    }
    explicit Node(const Item &data) : data(data){}

    Node *getNeighbour() const {
        return neighbour;
    }

    void setNeighbour(Node *neighbour) {
        Node::neighbour = neighbour;
    }

    Node *getChild() const {
        return child;
    }

    void setChild(Node *child) {
        Node::child = child;
    }

    Node *getParent() const {
        return parent;
    }

    void setParent(Node *parent) {
        Node::parent = parent;
    }

    Node *getPrevNeighbour() const {
        return prevNeighbour;
    }

    void setPrevNeighbour(Node *prevNeighbour) {
        Node::prevNeighbour = prevNeighbour;
    }

    Item getData() const {
        return data;
    }
};
class Chain{
public:
    Chain(int chainID,Chain * prevChain,Chain * nextChain): chainID(chainID),prevChain(prevChain),nextChain(nextChain){}
    int getChainId() const {
        return chainID;
    }
    void BHInsert(const Item& key){
        auto newNode= new Node(key);
        mainNode=BHMerge(mainNode,newNode);
        if(minNode == nullptr || minNode->getData()>key){
            minNode=newNode;
        }
    }
    bool BHInsert(Chain* mergChain){
        mainNode=BHMerge(mergChain->mainNode,mainNode);
        mergChain->mainNode= nullptr;
        mergChain->minNode = nullptr;
        minNode= nullptr;
        minNode= BHFindMin();
        return true;
    }
    bool end(){
        return nextChain == nullptr;
    }
    Chain *getPrevChain() const {
        return prevChain;
    }
    void setPrevChain(Chain *prevChain) {
        Chain::prevChain = prevChain;
    }
    Chain * getNextChain(){
        return nextChain;
    }
    void setNextChain(Chain *nextChain) {
        Chain::nextChain = nextChain;
    }

private:
    int chainID;
    Chain * prevChain;
    Chain * nextChain;
    Node * mainNode= nullptr;
    Node * minNode= nullptr;
private:
    Node * BHFindMin(){
        if(minNode!= nullptr){
            return minNode;
        }else if(mainNode== nullptr){
            return nullptr;
        }else{
            auto min = mainNode;
            auto now = mainNode;
            while (now->getNeighbour() != nullptr) {
                if (min->getData() > now->getData()) {
                    min = now;
                }
                now = now->getNeighbour();
            }
            if (min->getData() > now->getData()) {
                min = now;
            }
            minNode=min;
            return minNode;
        }
    }
    Node * BHExtractMin(){
        Node * newTree = nullptr;
        if(minNode->getPrevNeighbour()!= nullptr){
            minNode->getPrevNeighbour()->setNeighbour(minNode->getNeighbour());
        }
        if(minNode->getNeighbour()!= nullptr){
            minNode->getNeighbour()->setPrevNeighbour(minNode->getPrevNeighbour());
        }

        auto child=minNode->getChild();
        for(int i=minNode->getOrder();i>0;i--){
            auto nextChild=child->getPrevNeighbour();
            child->setParent(nullptr);
            if(nextChild!= nullptr) {
                child = nextChild;
            }
        }

        if(mainNode->getData()!=minNode->getData()){
            newTree=mainNode;
        }else{
            newTree=minNode->getNeighbour();
        }
        newTree=BHMerge(newTree,child);

        mainNode=newTree;

        auto min=minNode;
        minNode= nullptr;
        minNode=BHFindMin();
        return min;
    }
    Node * BHMergeTree(Node * T1,Node * T2,int order){
        if(T1== nullptr||T2== nullptr||T1->getParent()!= nullptr||T2->getParent()!= nullptr||(T1->getOrder()!=T2->getOrder())){
            return nullptr;
        }
        T2->setNeighbour(nullptr);
        T2->setPrevNeighbour(nullptr);
        T1->setNeighbour(nullptr);
        T1->setPrevNeighbour(nullptr);

        if(T1->getData()<=T2->getData()){
            T2->setParent(T1);
            T2->setPrevNeighbour(T1->getChild());
            if(T1->getChild()!= nullptr){
                T1->getChild()->setNeighbour(T2);
            }
            T1->setChild(T2);
            T1->setOrder(order);
            return T1;
        }else{
            T1->setParent(T2);
            T1->setPrevNeighbour(T2->getChild());
            if(T2->getChild()!= nullptr){
                T2->getChild()->setNeighbour(T1);
            }
            T2->setChild(T1);
            T2->setOrder(order);
            return T2;
        }

    }
    Node * BHMerge (Node * T1,Node * T2){
        if(T1== nullptr||T2== nullptr){
            if(T1 != nullptr){
                return T1;
            }else if(T2 != nullptr){
                return T2;
            }else{
                return nullptr;
            }
        }
        Node* head= nullptr;
        Node* prev= nullptr;
        Node* A=T1;
        Node* B=T2;
        Node* ANext;
        Node* BNext;
        if(T1!= nullptr)
            ANext=T1->getNeighbour();
        else
            ANext= nullptr;
        if(T2!= nullptr)
            BNext=T2->getNeighbour();
        else
            BNext= nullptr;
        Node* carry= nullptr;
        Node* carryNext= nullptr;
        int currentOrder=0;
        while(A != nullptr || B != nullptr){
            auto aOrder=-1;
            auto bOrder=-1;
            if(A != nullptr)
                aOrder=A->getOrder();
            if(B != nullptr)
                bOrder=B->getOrder();

            if(A!=nullptr && B!=nullptr &&(A->getOrder()==B->getOrder()&&currentOrder==A->getOrder())){
                carryNext=BHMergeTree(A,B,A->getOrder()+1);
            }else if(A != nullptr && carry != nullptr && A->getOrder() == currentOrder){
                carryNext=BHMergeTree(carry,A,A->getOrder()+1);
                carry= nullptr;
            }else if(B != nullptr&&carry != nullptr && B->getOrder() == currentOrder){
                carryNext=BHMergeTree(carry,B,B->getOrder()+1);
                carry= nullptr;
            }else if(A != nullptr&&carry == nullptr && A->getOrder() == currentOrder){
                carry=A;
            }else if(B != nullptr && carry == nullptr && B->getOrder() == currentOrder){
                carry=B;
            }else{
            }
            if(head == nullptr&&carry!= nullptr){
                head=carry;
                prev=carry;
            }else if(carry!= nullptr/* change is it required?*/) {
                prev->setNeighbour(carry);
                carry->setPrevNeighbour(prev);
                prev=carry;

            }
            carry=carryNext;
            carryNext= nullptr;
            currentOrder++;

            if(A != nullptr && aOrder<currentOrder) {
                A = ANext;
                if (ANext != nullptr)
                    ANext = A->getNeighbour();
            }
            if(B != nullptr && bOrder<currentOrder) {
                B = BNext;
                if (BNext != nullptr)
                    BNext = B->getNeighbour();
            }
        }
        if(head == nullptr&&carry!= nullptr){
            head=carry;
        }else if(carry!= nullptr/* change is it required?*/) {
            prev->setNeighbour(carry);
            carry->setPrevNeighbour(prev);


        }
        return head;
    }
//    Node * BHMerge (Node * T1,Node * T2){
//        if(T1== nullptr||T2== nullptr){
//            if(T1 != nullptr){
//                return T1;
//            }else if(T2 != nullptr){
//                return T2;
//            }else{
//                return nullptr;
//            }
//        }
//        if(T1==T2){
//            return T1;
//        }
//        Node* A = T1;
//        Node* B = T2;
//        Node* C = nullptr;
//        Node* head= nullptr;
//        Node * add[3];
//        Node * carry= nullptr;
//        int countOfNotEmpty = 2;
//        int order=0;
//
//
//        while (countOfNotEmpty >= 2){
//            countOfNotEmpty=0;
//            int numberOfEntitiesToAdd=0;
//
//            if(A!= nullptr){
//                countOfNotEmpty++;
//                if(A->getOrder() == order) {
//                    add[numberOfEntitiesToAdd] = A;
//                    numberOfEntitiesToAdd++;
//                    A=A->getNeighbour();
//                }
//            }
//            if(B!= nullptr){
//                countOfNotEmpty++;
//                if(B->getOrder() == order) {
//                    add[numberOfEntitiesToAdd] = B;
//                    numberOfEntitiesToAdd++;
//                    B=B->getNeighbour();
//                }
//            }
//            if(carry != nullptr){
//                countOfNotEmpty++;
//                add[numberOfEntitiesToAdd] = carry;
//                numberOfEntitiesToAdd++;
//                carry= nullptr;
//                }
//
//        switch(numberOfEntitiesToAdd) {
//            case 3:
//                if(C== nullptr){
//                    C=add[2];
//                    head=C;
//
//                }else{
//                    C->setNeighbour(add[2]);
//                    C->getNeighbour()->setPrevNeighbour(C);
//                }
//                carry= BHMergeTree(add[0], add[1], order + 1);
//                break;
//            case 2:
//                carry= BHMergeTree(add[0], add[1], order + 1);
//                break;
//            case 1:
//                if(C== nullptr){
//                    C=add[0];
//                    head=C;
//
//                }else{
//                    C->setNeighbour(add[0]);
//                    C->getNeighbour()->setPrevNeighbour(C);
//                }
//                break;
//            default:
//                break;
//        }
//            order++;
//            if(C!= nullptr && C->getNeighbour()!= nullptr){
//                C=C->getNeighbour();
//            }
//
//        }
//
//        while(A!= nullptr){
//            if(C== nullptr){
//                C=A;
//                head=C;
//            }else {
//                C->setNeighbour(A);
//                A->setPrevNeighbour(C);
//            }
//            A=A->getNeighbour();
//            }
//
//        while(B!= nullptr){
//            if(C== nullptr){
//                C=B;
//                head=C;
//
//            }else {
//                C->setNeighbour(B);
//                B->setPrevNeighbour(C);
//            }
//            B=B->getNeighbour();
//        }
//
//        return head;
//    }
public:
    Item findMin() {
        Node * node = BHFindMin();
        if(node == nullptr){
            return Item(0,0,0, false);
        }
        return node->getData();
    }
    Item removeMin(){
        if(mainNode== nullptr){
            return Item(0,0,0, false);
        }
        auto deletedItem=this->BHExtractMin();
        if(deletedItem == nullptr){
            return Item(0,0,0, false);
        }
        return deletedItem->getData();
    }
};

class CHolding
{
public:

    Chain * chains = nullptr;

    void Add (int chainID, unsigned int itemID, unsigned int revenue){
        auto chain = findChain(chainID, true);
        chain->BHInsert(Item(itemID, revenue, ++time, true));
    }
    bool Remove (int chainID, unsigned int & itemID){
        ////find chain
        auto chain = findChain(chainID,false);
        if(chain == nullptr){
            return false;
        }
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ////find item in chain and remove it and set itemID to itemID of item that was removed
        auto item = chain->removeMin();
        if(!item.isAlive()){
            return false;
        }
        itemID=item.getItemId();
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////
        return true;

    }
    bool Remove (unsigned int & itemID){
        //todo not checking min and id
        Item absoluteMinRevenue(0,0,0,false);
        int chainID=1000000000;
        bool firstTime=true;

        ////go through all the chains and find the minest min if no chains or no items keep chain -1
        auto tmp = chains;
        while (tmp != nullptr){
            auto min=tmp->findMin();
            if(min.isAlive() &&((absoluteMinRevenue.getRevenue()>=min.getRevenue()) || firstTime)){
                if(absoluteMinRevenue.getRevenue()==min.getRevenue()&&tmp->getChainId()<chainID){
                    firstTime=false;
                    absoluteMinRevenue=min;
                    chainID=tmp->getChainId();
                }else if(absoluteMinRevenue.getRevenue()!=min.getRevenue()){
                    firstTime=false;
                    absoluteMinRevenue=min;
                    chainID=tmp->getChainId();
                }
            }
            tmp = tmp->getNextChain();
        }
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ////check if some item exists
        if(chainID == 1000000000){
            return false;
        }
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ////remove minest min from chain and set itemID to itemID of chain that was removed
        auto chain = findChain(chainID,false);
        auto removedItem=chain->removeMin();
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////
        itemID=removedItem.getItemId();

        return true;
    }
    void Merge  (int dstChainID, int srcChainID){

        auto dstChain = findChain(dstChainID, true);
        auto srcChain = findChain(srcChainID,true);
        dstChain->BHInsert(srcChain);
        deleteChain(srcChainID);
    }
    bool Merge  (void){

        Item absoluteMinRevenue(0,0,0,false);
        int srcChainID=1000000000;
        bool firstTime=true;

        auto tmp = chains;
        while (tmp != nullptr){
            auto min=tmp->findMin();
            if(min.isAlive() &&((absoluteMinRevenue.getRevenue()>=min.getRevenue()) || firstTime)){
                if(absoluteMinRevenue.getRevenue()==min.getRevenue()&&tmp->getChainId()<srcChainID){
                    firstTime=false;
                    absoluteMinRevenue=min;
                    srcChainID=tmp->getChainId();
                }else if(absoluteMinRevenue.getRevenue()!=min.getRevenue()){
                    firstTime=false;
                    absoluteMinRevenue=min;
                    srcChainID=tmp->getChainId();
                }
            }
            tmp = tmp->getNextChain();
        }
        Item absoluteMinRevenue2(0,0,0,false);
        int dstChainID=1000000000;
        bool firstTime2=true;

        tmp = chains;
        while (tmp != nullptr){
            auto min=tmp->findMin();
            if(min==absoluteMinRevenue){
                tmp = tmp->getNextChain();
                continue;
            }
            if(min.isAlive() &&((absoluteMinRevenue2.getRevenue()>=min.getRevenue()) || firstTime2)){
                if(absoluteMinRevenue2.getRevenue()==min.getRevenue()&& tmp->getChainId() < dstChainID){
                    firstTime2=false;
                    absoluteMinRevenue2=min;
                    dstChainID=tmp->getChainId();
                }else if(absoluteMinRevenue2.getRevenue()!=min.getRevenue()){
                    firstTime2=false;
                    absoluteMinRevenue2=min;
                    dstChainID=tmp->getChainId();
                }
            }
            tmp = tmp->getNextChain();
        }
        if(dstChainID==1000000000||srcChainID==1000000000){
            return false;
        }
        Merge(srcChainID,dstChainID);
        return true;
    }
    ~CHolding()= default;
private:
    unsigned  long long int time=0;
    void deleteChain(int chainID){
        auto tmp = chains;
        if(tmp == nullptr){
            return;
        }
        while (!tmp->end()){
            if(tmp->getChainId()==chainID){
                break;
            }
            tmp = tmp->getNextChain();
        }

        auto prevNeighbour=tmp->getPrevChain();
        auto nextNeighbour=tmp->getNextChain();
        if(tmp==chains){
            chains=nextNeighbour;
            if(nextNeighbour!= nullptr){
                nextNeighbour->setPrevChain(nullptr);
            }
            return ;
        }
        prevNeighbour->setNextChain(nextNeighbour);
        if(nextNeighbour!= nullptr){
            nextNeighbour->setPrevChain(prevNeighbour);
        }
        delete(tmp);
    }
    Chain * createChain(int chainID){
        auto tmp = chains;
        if(tmp == nullptr){
            auto newTmp = new Chain(chainID, nullptr, nullptr);
            chains=newTmp;
            return newTmp;
        }
        while (!tmp->end()){
            tmp = tmp->getNextChain();
        }
        auto newTmp = new Chain(chainID, tmp, nullptr);
        tmp->setNextChain(newTmp);
        return newTmp;

    }
    Chain * findChain(int chainID, bool createIfNotFound){
        auto tmp = chains;
        if(tmp == nullptr&&createIfNotFound){
            return createChain(chainID);
        } else if(tmp == nullptr){
            return nullptr;
        }
        while (tmp != nullptr){

            if(tmp->getChainId()==chainID){
                break;
            }

            tmp = tmp->getNextChain();
        }
        if(tmp == nullptr&&createIfNotFound){
            return createChain(chainID);
        }
        return tmp;
    }

};

#ifndef __PROGTEST__
int test(){
    unsigned int id;
    bool res;
    CHolding c3;
    c3.Add(1, 1, 1);
    c3.Add(1, 2, 1);
    c3.Add(1, 3, 2);
    res = c3.Remove(id);
    assert(res && id == 1);
    res = c3.Remove(id);
    assert(res && id == 2);
    res = c3.Remove(id);
    assert(res && id == 3);
    c3.Add(1, 1, 1);
    c3.Add(1, 2, 1);
    c3.Add(1, 3, 2);
    c3.Add(2, 4, 1);
    c3.Add(2, 5, 1);
    c3.Add(2, 6, 2);
    res = c3.Remove(id);
    assert(res && id == 1);
    res = c3.Remove(id);
    assert(res && id == 2);
    res = c3.Remove(id);
    assert(res && id == 4);
    res = c3.Remove(id);
    assert(res && id == 5);
    res = c3.Remove(id);
    assert(res && id == 3);
    res = c3.Remove(id);
    assert(res && id == 6);

    CHolding c2;
    c2.Add(30, 80, 4);
    c2.Add(30, 83, 3);
    c2.Add(30, 82, 2);
    c2.Add(30, 81, 1);
    c2.Add(30, 181, 1);
    c2.Add(20, 90, 4);
    c2.Add(20, 93, 3);
    c2.Add(20, 92, 2);
    c2.Add(20, 291, 1);
    c2.Add(20, 191, 1);
//---
    res = c2.Remove(id);
    assert(res && id == 191);
    res = c2.Remove(id);
    assert(res && id == 291);
    res = c2.Remove(id);
    assert(res && id == 81);
    res = c2.Remove(id);
    assert(res && id == 181);
    res = c2.Remove(id);
    assert(res && id == 92);
    res = c2.Remove(id);
    assert(res && id == 82);
    res = c2.Remove(id);
    assert(res && id == 93);
    res = c2.Remove(id);
    assert(res && id == 83);
    res = c2.Remove(id);
    assert(res && id == 90);
    res = c2.Remove(id);
    assert(res && id == 80);
    res = c2.Remove(id);
    assert(!res && id == 80);
    return true;
}
int basicTests(){
    unsigned int id=666;
    CHolding f1;
    f1 . Add ( 7, 2, 9 );
    f1 . Add ( 12, 4, 4 );
    f1 . Add ( 6, 15, 2 );
    f1 . Add ( 6, 9, 3 );
    assert(f1.Remove(12, id) && id == 4);
    // res = true, id = 4
    id=666;
    assert(!f1 . Remove ( 12, id )&&id==666);
    // res = false, id = N/A
    assert(f1 . Remove ( 6, id )&&id==15);
    // res = true, id = 15
    assert(f1 . Remove ( 6, id )&&id==9);
    // res = true, id = 9
    id=666;
    assert(!f1 . Remove ( 6, id )&&id==666);
    // res = false, id = N/A


//Ukazkovy vstup #2
//-----------------
    CHolding f2;
    f2 . Add ( 4, 2, 2 );
    f2 . Add ( 1, 4, 3 );
    f2 . Add ( 8, 9, 8 );
    assert(f2 . Remove ( id )&&id==2);
    // res = true, id = 2
    assert(f2 . Remove ( id )&&id==4);
    // res = true, id = 4


//Ukazkovy vstup #3
//-----------------
    CHolding f3;
    f3 . Add ( 10, 101, 9 );
    f3 . Add ( 10, 102, 8 );
    f3 . Add ( 10, 103, 7 );
    f3 . Add ( 10, 104, 6 );
    f3 . Add ( 10, 105, 5 );
    f3 . Add ( 20, 201, 9 );
    f3 . Add ( 20, 202, 8 );
    f3 . Add ( 20, 203, 7 );
    f3 . Add ( 20, 204, 6 );
    f3 . Add ( 20, 205, 5 );
    f3 . Add ( 30, 301, 9 );
    f3 . Add ( 30, 302, 8 );
    f3 . Add ( 30, 303, 7 );
    f3 . Add ( 30, 304, 6 );
    f3 . Add ( 30, 305, 5 );
    assert(f3 . Remove ( id )&&id==105);
    // res = true, id = 105
    assert(f3 . Remove ( id )&&id==205);
    // res = true, id = 205
    assert(f3 . Remove ( id )&&id==305);
    // res = true, id = 305
    assert(f3 . Remove ( id )&&id==104);
    // res = true, id = 104
    assert(f3 . Remove ( id )&&id==204);
    // res = true, id = 204
    assert(f3 . Remove ( id )&&id==304);
    // res = true, id = 304
    assert(f3 . Remove ( id )&&id==103);
    // res = true, id = 103
    assert(f3 . Remove ( id )&&id==203);
    // res = true, id = 203
    assert(f3 . Remove ( id )&&id==303);
    // res = true, id = 303
    assert(f3 . Remove ( id )&&id==102);
    // res = true, id = 102
    assert(f3 . Remove ( id )&&id==202);
    // res = true, id = 202
    assert(f3 . Remove ( id )&&id==302);
    // res = true, id = 302

    return 0;
}
int main() {
    basicTests();
    test();

    unsigned int id=666;

//Ukazkovy vstup #4
//-----------------
    CHolding f4;
    f4 . Add ( 10, 101, 9 );
    f4 . Add ( 10, 102, 8 );
    f4 . Add ( 10, 103, 7 );
    f4 . Add ( 10, 104, 6 );
    f4 . Add ( 10, 105, 5 );
    f4 . Add ( 20, 201, 9 );
    f4 . Add ( 20, 202, 8 );
    f4 . Add ( 20, 203, 7 );
    f4 . Add ( 20, 204, 6 );
    f4 . Add ( 20, 205, 5 );
    f4 . Add ( 30, 301, 9 );
    f4 . Add ( 30, 302, 8 );
    f4 . Add ( 30, 303, 7 );
    f4 . Add ( 30, 304, 6 );
    f4 . Add ( 30, 305, 5 );
    assert(f4 . Remove ( 30, id )&&id==305);
    // res = true, id = 305
    assert(f4 . Remove ( 20, id )&&id==205);
    // res = true, id = 205
    assert(f4 . Remove ( 10, id )&&id==105);
    // res = true, id = 105
    f4 . Merge ( 30, 10 );
    id=666;
    assert(!f4 . Remove ( 10, id )&&id==666);
    // res = false, id = N/A
    assert(f4 . Remove ( 20, id )&&id==204);
    // res = true, id = 204
    assert(f4 . Remove ( 30, id )&&id==104);
    // res = true, id = 104
    assert(f4 . Remove ( id )&&id==304);
    // res = true, id = 304
    assert(f4 . Remove ( id )&&id==203);
    // res = true, id = 203
    assert(f4 . Remove ( id )&&id==103);
    // res = true, id = 103
    assert(f4 . Remove ( id )&&id==303);
    // res = true, id = 303
    assert(f4 . Remove ( id )&&id==202);
    // res = true, id = 202
    assert(f4 . Remove ( id )&&id==102);
    // res = true, id = 102
    assert(f4 . Remove ( id )&&id==302);
    // res = true, id = 302
    assert(f4 . Remove ( id )&&id==201);
    // res = true, id = 201
    assert(f4 . Remove ( id )&&id==101);
    // res = true, id = 101
    assert(f4 . Remove ( id )&&id==301);
    // res = true, id = 301
    id=666;
    assert(!f4 . Remove ( id )&&id==666);
    // res = false, id = N/A
    assert(!f4 . Remove ( id )&&id==666);
    // res = false, id = N/A


//Ukazkovy vstup #5
//-----------------
    CHolding f5;
    f5 . Add ( 10, 333, 5 );
    f5 . Add ( 20, 444, 2 );
    f5 . Add ( 10, 222, 6 );
    f5 . Add ( 20, 555, 8 );
    assert(f5 . Remove ( 10, id )&&id==333);
    // res = true, id = 333
    assert(f5 . Remove ( id )&&id==444);
    // res = true, id = 444
    f5 . Merge ( 20, 10 );
    id=666;
    assert(!f5 . Remove ( 10, id )&&id==666);
    // res = false, id = N/A
    assert(f5 . Remove ( 20, id )&&id==222);
    // res = true, id = 222


//Ukazkovy vstup #6
//-----------------
    CHolding f6;
    f6 . Add ( 10, 1, 7 );
    f6 . Add ( 20, 1, 7 );
    f6 . Add ( 30, 1, 7 );
    assert(f6 . Merge ( ));
    // res = true
    id=666;
    assert(!f6 . Remove ( 20, id )&&id==666);
    // res = false, id = N/A
    assert(f6 . Remove ( 30, id )&&id==1);
    // res = true, id = 1
    id=666;
    assert(!f6 . Remove ( 30, id )&&id==666);
    // res = false, id = N/A
    assert(f6 . Remove ( 10, id )&&id==1);
    // res = true, id = 1
    assert(f6 . Remove ( 10, id )&&id==1);
    // res = true, id = 1
    id=666;
    assert(!f6 . Remove ( 10, id )&&id==666);
    // res = false, id = N/A


//Ukazkovy vstup #7
//-----------------
    CHolding f7;
    f7 . Add ( 1, 1, 1 );
    f7 . Add ( 2, 2, 1 );
    f7 . Add ( 3, 3, 1 );
    assert(f7 . Merge ( ));
    // res = true
    assert(f7 . Merge ( ));
    // res = true
    assert(!f7 . Merge ( ));
    // res = false
    assert(f7 . Remove ( id )&&id==1);
    // res = true, id = 1
    assert(f7 . Remove ( id )&&id==2);
    // res = true, id = 2
    assert(f7 . Remove ( id )&&id==3);
    // res = true, id = 3
    id=666;
    assert(!f7 . Remove ( id )&&id==666);
    // res = false, id = N/A




    return 0;
}
#endif /* __PROGTEST__ */